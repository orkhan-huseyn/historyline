import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {RestService} from '../rest.service';
import {DataService} from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public searchTerm = '';
  public virtualServer = 'http://virtualserver32.cloudapp.net:8080/article-api/autocomplete?query=:searchTerm';

  categories = [];
  isFetching = false;

  constructor(private router: Router, private service: RestService, private data: DataService) { }

  ngOnInit() {
    this.getCategoryCounts();
  }

  onArticleSelect(article) {
    article instanceof Object ? this.router.navigate(['/article', article.id]) : false;
  }

  search(text) {
    this.router.navigate(['/search'], {queryParams: {q: text, searchInContent: true}});
  }

  searchByButton(){
    this.router.navigate(['/search'], {queryParams: {q: this.searchTerm}});
  }

  format(item: any) {
    return `<span class="autocomplete-list-item">
              <i class="fa fa-newspaper-o"></i>
              ${item.name}
            </span>`;
  }

  async getCategoryCounts() {
    try {
      this.isFetching = true;
      const data = await this.service.get('http://virtualserver32.cloudapp.net:8080/article-api/statsByCategory');

      this.categories = data as any[];
      this.isFetching = false;

    } catch (e) {
      this.isFetching = false;
      this.data.error(e['message']);
    }
  }

}
