import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RestService} from '../rest.service';
import {DataService} from '../data.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  id: number;
  article: any;

  isFetching = false;

  constructor(private route: ActivatedRoute, private service: RestService, private data: DataService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.getArticleDetails();
  }

  async getArticleDetails() {

    try {
      this.isFetching = true;
      const data = await this.service.get(
        'http://virtualserver32.cloudapp.net:8080/article-api/articleById?id='+this.id
      );

      this.article = data as any;
      this.isFetching = false;

      if (this.article==null)
        this.data.info('Axtardığınız məqalə tapılmadı');

    } catch (e) {
      this.data.error(e['message']);
    }
  }

}
