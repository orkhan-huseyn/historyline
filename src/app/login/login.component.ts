import {Component, OnDestroy, OnInit, Inject} from '@angular/core';
import { Router } from '@angular/router';

import { RestService } from '../rest.service';
import { DataService } from '../data.service';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  username = '';
  password = '';

  btnDisabled = false;

  constructor(private service: RestService, private data: DataService, private router: Router, @Inject(DOCUMENT) private document: Document) {}

  ngOnInit() {
    this.document.body.classList.add('layout-empty');
  }

  ngOnDestroy() {
    this.document.body.classList.remove('layout-empty');
  }

  validate() {
    if (this.username) {
      if (this.password) {
        return true;
      } else {
        this.data.error('Şifrəni daxil edin');
      }
    } else {
      this.data.error('İstifadəçi adını daxil edin');
    }
  }

  async login() {

    try {

      if (this.validate()) {
        this.data.message = '';
        this.btnDisabled = true;

        const data = await this.service.post(
          'http://virtualserver32.cloudapp.net:8080/user-api/login',
          {
            username: this.username,
            password: this.password
          }
        );


        if (data['error']) {
          this.data.error(data['error']);
        } else {
          this.data.user = data['data']['user'];
          this.data.token = data['data']['token'];
          this.router.navigate(['/profile']);
        }

        this.btnDisabled = false;

      }

    } catch (e) {
      this.data.error(e['message']);
    }
  }

}
