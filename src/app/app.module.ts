import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { IonRangeSliderModule } from "ng2-ion-range-slider";
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { MessageComponent } from './message/message.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { ProfileComponent } from './profile/profile.component';


import { DataService } from './data.service';
import { RestService } from './rest.service';
import { AuthGuardService } from './auth-guard.service';
import { AboutComponent } from './about/about.component';
import { ArticleComponent } from './article/article.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { AdminMenuComponent } from './admin-menu/admin-menu.component';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { MyTimelineComponent } from './my-timeline/my-timeline.component';
import { SettingsComponent } from './settings/settings.component';
import { TimelinesComponent } from './timelines/timelines.component';
import { TimelineComponent } from './timeline/timeline.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    RegistrationComponent,
    LoginComponent,
    MessageComponent,
    SearchResultsComponent,
    ProfileComponent,
    AboutComponent,
    ArticleComponent,
    FooterComponent,
    HeaderComponent,
    ResetPasswordComponent,
    AdminMenuComponent,
    AdminHeaderComponent,
    TermsAndConditionsComponent,
    MyTimelineComponent,
    SettingsComponent,
    TimelinesComponent,
    TimelineComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    IonRangeSliderModule,
    NguiAutoCompleteModule
  ],
  providers: [DataService, RestService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
