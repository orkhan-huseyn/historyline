import { Component, OnInit } from '@angular/core';


import { DataService} from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menuOpen = false;

  constructor(public data: DataService, private router: Router) {}

  logout() {
    this.data.user = null;
    this.data.invalidateToken();
    this.router.navigate(['/']);
  }

  ngOnInit() {
  }

  toggleMenu() {
    this.menuOpen = !this.menuOpen;
  }

}
