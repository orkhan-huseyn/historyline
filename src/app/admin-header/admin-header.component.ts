import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DataService} from '../data.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.scss']
})
export class AdminHeaderComponent implements OnInit {

  constructor(public data: DataService, private router: Router) {}

  ngOnInit() {
  }

  logout() {
    this.data.user = null;
    this.data.invalidateToken();
    this.router.navigate(['/']);
  }

}
