import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { DataService } from './data.service';

@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private data: DataService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.data.token) {
      return state.url.startsWith('/profile') ? true : (this.router.navigate(['/']), false);
    } else {
      return state.url.startsWith('/profile') ? (this.router.navigate(['/']), false) : true;
    }
  }

}
