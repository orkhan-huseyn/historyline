import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  name: string;
  surname: string;
  location: string;

  constructor(private service: RestService, private data: DataService) { }

  ngOnInit() {
    this.getUserInfo();
  }

  async getUserInfo() {
    try {
      const data = await this.service.get('http://virtualserver32.cloudapp.net:8080/user-api/profile');
      this.name = data['name'];
      this.surname = data['surname'];
      this.location = data['locationId']['name'];
    } catch (e) {
      this.data.error(e['message']);
    }
  }

}
