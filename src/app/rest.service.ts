import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class RestService {

  constructor(private http: HttpClient) { }

  getHeaders() {
    const token = localStorage.getItem('token');
    return token ? new HttpHeaders().set('Authorization', 'Bearer ' + token) : null;
  }

  get(url: string) {
    return this.http.get(url, { headers: this.getHeaders() }).toPromise();
  }

  post(url: string, body: any) {
    return this.http.post(url, body,{ headers: this.getHeaders() }).toPromise();
  }

  put(url: string, body: any) {
    return this.http.put(url, body,{ headers: this.getHeaders() }).toPromise();
  }

  delete(url: string) {
    return this.http.delete(url, { headers: this.getHeaders() }).toPromise();
  }

}
