import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';
import {DataService} from '../data.service';

@Component({
  selector: 'app-my-timeline',
  templateUrl: './my-timeline.component.html',
  styleUrls: ['./my-timeline.component.scss']
})
export class MyTimelineComponent implements OnInit {

  constructor(private service: RestService, private data: DataService) { }

  ngOnInit() {
    this.data.info('Sizin tarixçəniz yoxdur');
  }

  async getTimeline() {
    try {

    } catch (e) {
      this.data.error(e['message']);
    }
  }

}
