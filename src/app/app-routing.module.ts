import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { TermsAndConditionsComponent } from './terms-and-conditions/terms-and-conditions.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AboutComponent } from './about/about.component';
import { ArticleComponent } from './article/article.component';
import { SettingsComponent } from './settings/settings.component';
import { MyTimelineComponent } from './my-timeline/my-timeline.component';

import { AuthGuardService } from './auth-guard.service';
import { TimelinesComponent } from './timelines/timelines.component';
import { TimelineComponent } from './timeline/timeline.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'about',
    component: AboutComponent
  },
  {
    path: 'terms-and-conditions',
    component: TermsAndConditionsComponent
  },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'registration',
    component: RegistrationComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'reset-password',
    component: ResetPasswordComponent
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'profile/settings',
    component: SettingsComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'profile/my-timeline',
    component: MyTimelineComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'search',
    component: SearchResultsComponent
  },
  {
    path: 'timeline/:id',
    component: TimelineComponent
  },
  {
    path: 'timelines',
    component: TimelinesComponent
  },
  {
    path: 'article/:id',
    component: ArticleComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
