import {Component, Inject, OnDestroy, OnInit} from '@angular/core';

import { RestService } from '../rest.service';
import { DataService } from '../data.service';
import {DOCUMENT} from '@angular/common';
import {Router} from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit, OnDestroy {

  username = '';
  password = '';
  passwordConf = '';
  email = '';
  name = '';
  surname = '';
  locationId = 1;
  agreed = false;

  locations = [];

  btnDisabled = false;

  constructor(private service: RestService, private data: DataService, @Inject(DOCUMENT) private document: Document, private router: Router) {
  }

  ngOnInit() {
    this.document.body.classList.add('layout-empty');
    this.getLocations();
  }

  ngOnDestroy() {
    this.document.body.classList.remove('layout-empty');
  }

  validate() {
    if (this.username) {
      if (this.password) {
        if (this.password === this.passwordConf) {
          if (this.name) {
            if (this.surname) {
              if (this.email) {
                if (this.validateEmail(this.email)) {
                  return true;
                } else {
                  this.data.error('E-poçt formata uyğun deyil');
                }
              } else {
                this.data.error('E-poçt ünvanınızı daxil edin');
              }
            } else {
              this.data.error('Soyadınızı daxil edin');
            }
          } else {
            this.data.error('Adınızı daxil edin');
          }
        } else {
          this.data.error('Şifrələr uyğunlaşmır');
        }
      } else {
        this.data.error('Şifrəni daxil edin');
      }
    } else {
      this.data.error('İstifadəçi adınızı daxil edin');
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  async register() {

    try {

      if (this.validate()) {
        this.data.message = '';
        this.btnDisabled = true;

        const data = await this.service.post(
          'http://virtualserver32.cloudapp.net:8080/user-api/register',
          {
            username: this.username,
            password: this.password,
            name: this.name,
            surname: this.surname,
            email: this.email,
            locationId: +this.locationId
          }
        );

        if (data['error']) {
          this.data.error(data['error']);
        } else {
          this.data.success('Qeydiyyat uğurla başa çatdı. Hesabınıza yönləndirilirsiniz...');
          setTimeout(() => {
            this.router.navigate(['/login']);
          }, 3000);
        }

        this.btnDisabled = false;

      }

    } catch (e) {
      this.data.error(e['message']);
    }
  }

  async getLocations() {
    try {

      const data = await this.service.get(
        'http://virtualserver32.cloudapp.net:8080/common-api/locations'
      );

      this.locations = data as any[];

    } catch (e) {
      this.data.error(e['message']);
    }
  }

}
