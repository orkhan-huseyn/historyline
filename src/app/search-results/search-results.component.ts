import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute } from '@angular/router';
import {DataService} from '../data.service';

import * as moment from 'moment';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnInit {

  searchResults: any[];
  startDate: any;
  endDate: any;
  searchTerm = '';
  category: number;
  location: number;
  author: number;
  searchInContent: boolean;

  locations = [];
  categories = [];
  authors = [];

  minInterval = 86400000;

  isFetching = false;
  currenctPage = 1;
  limit = 5;
  offset = 0;
  total = 0;
  pageNums = [];

  constructor(private service: RestService, private activatedRoute: ActivatedRoute, private data: DataService) { }

  ngOnInit() {

    const params = this.activatedRoute.snapshot.queryParams;
    this.searchTerm = params['q'];
    this.category = params['category'];
    this.author = params['author'];
    this.location = params['location'];
    this.searchInContent = params['searchInContent'] != undefined && params['searchInContent'] == 'true';
    this.momentI18n();
    this.getDateInterval();
    this.getAuthors();
    this.getCategories();
    this.getLocations();

    if (this.searchTerm != undefined || this.category != undefined || this.author != undefined || this.location != undefined) {
      this.getArticles();
    }
  }

  pagination() {
    const numOfPages = Math.ceil(this.total / this.limit);
    this.pageNums = Array(numOfPages).fill(0).map((x, i) => i+1);
  }

  goToPage(num) {
    this.currenctPage = num;
    this.offset = (num - 1) * this.limit;
    this.getArticles();
  }

  search() {
    this.data.message = '';
    this.getArticles();
  }

  reset() {
    this.author = undefined;
    this.category = undefined;
    this.location = undefined;
    this.searchInContent = false;
    this.searchTerm = '';
    this.searchResults = [];
    this.total = 0;
    this.offset = 0;
    this.currenctPage = 1;
    this.data.message = '';
    this.pagination();
    this.getDateInterval();
  }

  onFinish(event) {
    this.startDate = event.from;
    this.endDate = event.to;
    this.getArticles();
  }

  prettify(num) {
    return moment(num, 'X').format('LL');
  }

  momentI18n() {
    moment.updateLocale('az', {
      months: [
        'Yanvar', 'Fevral', 'Mart', 'Aprel', 'May', 'İyun', 'İyul', 'Avqust', 'Sentyabr', 'Oktyabr', 'Noyabr', 'Dekabr',
      ],
      monthsShort : [
        'Yan', 'Fev', 'Mart', 'Apr', 'May', 'İyun', 'İyul', 'Avq', 'Sent', 'Okt', 'Noy', 'Dek',
      ],
      monthsParseExact : true,
      weekdays : [
        'Bazar', 'Bazar ertəsi', 'Çərşənbə axşamı', 'Çərşənbə', 'Cümə axşamı', 'Cümə', 'Şənbə',
      ],
      weekdaysShort : [
        'B', 'BE', 'ÇA', 'Ç', 'CA', 'C', 'Ş'
      ],
      weekdaysMin : [
        'B', 'BE', 'ÇA', 'Ç', 'CA', 'C', 'Ş'
      ],
      weekdaysParseExact : true,
      longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd D MMMM YYYY HH:mm'
      }
    });
  }

  async getDateInterval() {
    try {
      const data = await this.service.get('http://virtualserver32.cloudapp.net:8080/article-api/dates');

      this.startDate = +moment(data['min'], 'YYYY-MM-DD').format('X');
      this.endDate = +moment(data['max'], 'YYYY-MM-DD').format('X');

    } catch (e) {
      this.data.error(e['message']);
    }
  }

  async getLocations() {
    try {

      const data = await this.service.get(
        'http://virtualserver32.cloudapp.net:8080/common-api/locations'
      );

      this.locations = data as any[];

    } catch (e) {
      this.data.error(e['message']);
    }
  }

  async getCategories() {
    try {

      const data = await this.service.get(
        'http://virtualserver32.cloudapp.net:8080/common-api/categories'
      );

      this.categories = data as any[];

    } catch (e) {
      this.data.error(e['message']);
    }
  }

  async getAuthors() {
    try {

      const data = await this.service.get(
        'http://virtualserver32.cloudapp.net:8080/user-api/list'
      );

      this.authors = data as any[];

    } catch (e) {
      this.data.error(e['message']);
    }
  }

  async getArticles() {

    try {

      this.isFetching = true;

      let url = `http://virtualserver32.cloudapp.net:8080/article-api/search?limit=${this.limit}&offset=${this.offset}`;
          url += this.searchTerm != undefined ? `&query=${this.searchTerm}&searchInContent=${this.searchInContent}` : '';
          url += this.startDate != undefined ? `&startDate=${moment(this.startDate, 'X').format('YYYY-MM-DD')}` : '';
          url += this.endDate != undefined ? `&endDate=${moment(this.endDate, 'X').format('YYYY-MM-DD')}` : '';
          url += this.location != undefined ? `&location=${this.location}` : '';
          url += this.category != undefined ? `&category=${this.category}` : '';
          url += this.author != undefined ? `&author=${this.author}` : '';

      const data = await this.service.get(url);

      this.searchResults = data['list'] as any[];
      this.total = data['totalCount'] as number;
      this.isFetching = false;

      this.pagination();

      if (this.searchResults.length == 0)
        this.data.info('Axtarışa uyğun nəticə tapılmadı');

    } catch (e) {
      this.isFetching = false;
      this.data.error(e['message']);
    }

  }

}
