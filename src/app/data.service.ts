import {Injectable} from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Injectable()
export class DataService {

  message = '';
  messageType = 'danger';

  user:any;

  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.message = '';
      }
    });
  }

  set token(token) {
    localStorage.setItem('token', token);
  }

  get token() {
    return localStorage.getItem('token');
  }

  invalidateToken() {
    localStorage.removeItem('token');
  }

  error(message) {
    this.messageType = 'danger';
    this.message = message;
  }

  success(message) {
    this.messageType = 'success';
    this.message = message;
  }

  warning(message) {
    this.messageType = 'warning';
    this.message = message;
  }

  info(message) {
    this.messageType = 'info';
    this.message = message;
  }

}
