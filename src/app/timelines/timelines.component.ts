import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { DataService } from '../data.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-timelines',
  templateUrl: './timelines.component.html',
  styleUrls: ['./timelines.component.scss']
})
export class TimelinesComponent implements OnInit {

  searchResults: any[];
  searchTerm = '';
  category: number;

  isFetching = false;
  currenctPage = 1;
  limit = 5;
  offset = 0;
  total = 0;
  pageNums = [];

  constructor(private service: RestService, private data: DataService, private route: ActivatedRoute ) { }

  ngOnInit() {
    const params = this.route.snapshot.queryParams;
    this.searchTerm = params['q'];
    this.category = params['category'];
    if (this.searchTerm || this.category) {
      this.getPages();
    }
  }

  pagination() {
    const numOfPages = Math.ceil(this.total / this.limit);
    this.pageNums = Array(numOfPages).fill(0).map((x, i) => i+1);
  }

  goToPage(num) {
    this.currenctPage = num;
    this.offset = (num - 1) * this.limit;
    this.getPages();
  }

  search(category: number = null) {
    this.data.message = '';
    category != null ? this.category = category : false;
    this.getPages();
  }

  async getPages() {
    try {

      this.isFetching = true;

      let url = `http://virtualserver32.cloudapp.net:8080/page-api/pages?limit=${this.limit}&offset=${this.offset}`;
      url += this.searchTerm ? `&q=${this.searchTerm}` : '';
      url += this.category ? `&category=${this.category}` : '';

      const data = await this.service.get(url);

      this.searchResults = data['list'] as any[];
      this.total = data['totalCount'] as number;
      this.isFetching = false;

      this.pagination();

      if (this.searchResults.length == 0)
        this.data.info('Axtarışa uyğun nəticə tapılmadı');

    } catch (e) {
      this.isFetching = false;
      this.data.error(e['message']);
    }
  }

}
